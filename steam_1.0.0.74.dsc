-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (native)
Source: steam
Binary: steam-launcher, steam, steam-libs-amd64, steam-libs-i386
Architecture: all i386 amd64
Version: 1:1.0.0.74
Maintainer: Valve Corporation <linux@steampowered.com>
Homepage: http://www.steampowered.com/
Standards-Version: 3.9.4
Build-Depends: debhelper (>= 8.0.0), dh-python | python3 (<< 3.3.2-10~), perl, python3
Package-List: 
 steam deb oldlibs extra
 steam-launcher deb games optional
 steam-libs-amd64 deb games optional
 steam-libs-i386 deb games optional
Checksums-Sha1: 
 73eac39170beb44591979e860b8a347134d0cc98 3604858 steam_1.0.0.74.tar.gz
Checksums-Sha256: 
 b0ed3b8378f541e8dab68d8b589d85ad6dc0cdf302701cf8e87130eda2b1a234 3604858 steam_1.0.0.74.tar.gz
Files: 
 67c5d2352299aadfce6c7cd87098b700 3604858 steam_1.0.0.74.tar.gz

-----BEGIN PGP SIGNATURE-----

iQFLBAEBCgA1FiEEuhgW7451AF/PXieh8krqn7BUmLcFAmGnqIoXHGxpbnV4QHN0
ZWFtcG93ZXJlZC5jb20ACgkQ8krqn7BUmLcEYAf/RhE0C9F8m7N/tEbtaggUBQ25
cdoqI2WBcYa+HAUSiSb0bm9R6tmxZXAagasoGyk54gN6hDa+V1FBjdj/4i5o1eoq
+/QOl5dUCUwxnstc4qC8JLnWUZtnxRiCmTchzU0ICehx5D8snpGy8i80UZLWGIFb
ukVK6Kd9GvUE2R3m/bDmVW6xk1Mq6H57xRLWvDGATjkJZ8CyBZKUHtKpHkCR8dMS
oP8MPCsqbYHEWnQ0hNqgh3IaqB06YrSVfONUbJHbWxQiN6e0yrVAwF/IcX8li1MV
UlZL/4U3J97r3HoE+NOQ4HFyFta/ph9pk5RJiPhdN+7OaFcpRb897Lj4LbtduA==
=Lbz6
-----END PGP SIGNATURE-----
